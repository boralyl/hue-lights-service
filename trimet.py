from datetime import datetime, timedelta

from flask import current_app
import pylibmc
import requests

import constants


SCHEDULES = {
    "me": {
        "start": datetime(2014, 1, 1, 7, 8, 0),
        "end": datetime(2014, 1, 1, 7, 36, 0),
        "stop_id": constants.TRIMET_STOP_ID,
        "minutes": 7
    }
}
GONE_CACHE_KEY = "gone:{name}:v1"
GONE_CACHE_DURATION = int(timedelta(hours=3).total_seconds())A

cache = pylibmc.Client(["127.0.0.1"], binary=True)


def get_delta(estimated, now):
    """Gets the delta in minutes between estimated and now"""
    current_app.logger.debug("Now: %s", now)
    delta = (estimated - now)
    current_app.logger.debug("Delta: %s", delta)
    minutes = delta.seconds // 60
    current_app.logger.debug("Delta Minutes: %s", minutes)
    current_app.logger.debug("Estimated DateTime: %s", estimated)
    # if the estimated time is in the past, return 0
    if estimated < now:
        minutes = 0
    return minutes


def to_datetime_obj(date_string):
    """Converts the trimet date string into a datetime object"""
    trimmed_date = date_string[:19]
    return datetime.strptime(trimmed_date, '%Y-%m-%dT%H:%M:%S')


def should_perform_alert():
    """
    Determines if an alert should be sent for any existing configurations
    """
    # Don't check alerts if it's out of the time schedule
    now = datetime.now()
    start = SCHEDULES['me']['start']
    start_time = start.replace(year=now.year, month=now.month, day=now.day)
    end = SCHEDULES['me']['end']
    end_time = end.replace(year=now.year, month=now.month, day=now.day)
    alert_minutes = SCHEDULES['me']['minutes']
    stop_id = SCHEDULES['me']['stop_id']
    if now < start_time or now > end_time:
        current_app.logger.debug("out of range")
        return False

    params = {
        'json': True,
        'locIDs': stop_id,
        'appID': constants.TRIMET_APP_ID
    }
    response = requests.get(constants.TRIMET_ARRIVALS_API_URL, params=params)
    data = response.json()['resultSet']['arrival']
    current_app.logger.debug("Response: %s", response.json())
    arrival_times = [(t['block'], to_datetime_obj(t['estimated'])) for t in data]
    current_app.logger.debug("Arrival Times: %s", arrival_times)

    cache_key = GONE_CACHE_KEY.format(name='me')
    buses_departed = cache.get(cache_key) or []
    minutes = 0
    for block, arrival_time in arrival_times:
        # Block appears tobe a unique identifier for a bus.  Don't alert again
        # if we already alerted for that bus
        if block in buses_departed:
            continue
        minutes = get_delta(arrival_time, now)
        if minutes < alert_minutes:
            continue
        else:
            break

    should_alert = minutes == alert_minutes
    if should_alert:
        # Cache this bus so we don't alert more than once
        if block not in buses_departed:
            buses_departed.append(block)
            cache.set(cache_key, buses_departed, time=GONE_CACHE_DURATION)
    current_app.logger.debug("Buses departed: %s", buses_departed)

    return should_alert


if __name__ == '__main__':
    print "should_alert", should_perform_alert()
