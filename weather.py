from flask import current_app
import requests

import constants


ZERO_DEG_CELCIUS_KELVIN = 273


def get_brightness_for_cloudiness(cloudiness):
    """Returns a dict of data to send to lights based on cloudiness"""
    color = constants.WEATHER_CLOUD_COLOR
    percent = cloudiness * .01
    delta = percent * constants.MAX_BRIGHTNESS
    brightness = int(constants.MAX_BRIGHTNESS - delta)
    color['bri'] = brightness
    return color


def get_color_for_temp(temp):
    """Returns a dict of data to send to lights based on temp"""
    if temp <= 39:
        return constants.WEATHER_TEMP_COLORS['cold']
    elif temp > 39 and temp < 50:
        return constants.WEATHER_TEMP_COLORS['cool']
    elif temp >= 50 and temp < 65:
        return constants.WEATHER_TEMP_COLORS['mild']
    elif temp >= 65 and temp <= 80:
        return constants.WEATHER_TEMP_COLORS['warm']
    else:
        return constants.WEATHER_TEMP_COLORS['hot']


def kelvin_to_fahrenheit(kelvin):
    """Converts kelvin unit to fahrenheit"""
    celcius = kelvin - ZERO_DEG_CELCIUS_KELVIN
    return (celcius * 1.8000) + 32


def get_weather():
    """Retrieves weather and returns dict of light data based on weather"""
    light_states = {}
    response = requests.get(constants.WEATHER_API_URL)
    try:
        data = response.json()
    except ValueError:
        return "Error parsing json.  Status Code: %s" % (response.status_code,)

    # Set color for temperature
    temp = kelvin_to_fahrenheit(data['main']['temp'])
    temp_color = get_color_for_temp(temp)
    light_states[constants.LIVING_ROOM_COMPUTER_LIGHT_ID] = temp_color

    # Set color for weather
    current_app.logger.debug("Weather: %s", data['weather'])
    weather = data['weather'][0]
    weather_color = constants.WEATHER_CODES.get(weather['id'])
    light_states[constants.SOME_LIGHT_ID] = weather_color
    light_states[constants.SOME_OTHER_LIGHT_ID] = weather_color

    # Set color for cloudiness
    cloudiness = data['clouds']['all']
    brightness_color = get_brightness_for_cloudiness(cloudiness)
    light_states[constants.ANOTHER_LIGHT_ID] = brightness_color

    return light_states


if __name__ == '__main__':
    get_weather()
