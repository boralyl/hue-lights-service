BASE_URL = "http://127.0.0.1/api/your-hue-hub-id" # Your ip address of the hub should be used here
LIGHT_URL = BASE_URL + "/lights/{0}"
LIGHT_STATE_URL = BASE_URL + "/lights/{0}/state"
GROUP_STATE_URL = BASE_URL + "/groups/{0}/action"

LIGHT_IDS = [1, 2, 3, 5, 6, 7, 8, 9, 10]
MAIN_LIGHT_IDS = [1, 2, 3, 9, 10]
SOME_LIGHT_ID = 1
SOME_OTHER_LIGHT_ID = 2
ANOTHER_LIGHT_ID = 3

ALL_LIGHTS_GROUP_ID = 0
MAIN_LIGHTS_GROUP_ID = 1

MAX_BRIGHTNESS = 254

TRIMET_ARRIVALS_API_URL = 'http://developer.trimet.org/ws/V1/arrivals'
TRIMET_APP_ID = 'your-api-key'
TRIMET_STOP_ID = 123456789 # your trimet stop id

WEATHER_API_URL = 'http://api.openweathermap.org/data/2.5/weather?id=5746545&type=json'
WEATHER_CLOUD_COLOR = {
    'on': True,
    'hue': 33878,
    'bri': 254,
    'sat': 55
}
WEATHER_TEMP_COLORS = {
    'cold': {
        'on': True,
        'hue': 46638,
        'sat': 253,
        'bri': 254
    },
    'cool': {
        'on': True,
        'hue': 30026,
        'sat': 254,
        'bri': 254
    },
    'mild': {
        'on': True,
        'hue': 11393,
        'sat': 255,
        'bri': 254
    },
    'warm': {
        'on': True,
        'hue': 5268,
        'sat': 254,
        'bri': 255
    },
    'hot': {
        'on': True,
        'hue': 0,
        'sat': 254,
        'bri': 254
    },
}
WEATHER_CODES = {
    200: {
        'description': 'thunderstorm with light rain',
        "on": True,
        'hue': 46638,
        'sat': 253,
        'bri': 254
    },
    201: {
        'description': 'thunderstorm with rain',
        "on": True,
        'hue': 46638,
        'sat': 253,
        'bri': 254
    },
    202: {
        'description': 'thunderstorm with heavy rain',
        "on": True,
        'hue': 46638,
        'sat': 253,
        'bri': 254
    },
    210: {
        'description': 'light thunderstorm',
        "on": True,
        'hue': 46638,
        'sat': 253,
        'bri': 254
    },
    211: {
        'description': 'thunderstorm',
        "on": True,
        'hue': 46638,
        'sat': 253,
        'bri': 254
    },
    212: {
        'description': 'heavy thunderstorm',
        "on": True,
        'hue': 46638,
        'sat': 253,
        'bri': 254
    },
    230: {
        'description': 'thunderstorm with light drizzle',
        "on": True,
        'hue': 46638,
        'sat': 253,
        'bri': 254
    },
    231: {
        'description': 'thunderstorm with drizzle',
        "on": True,
        'hue': 46638,
        'sat': 253,
        'bri': 254
    },
    232: {
        'description': 'thunderstorm with heavy drizzle',
        "on": True,
        'hue': 46638,
        'sat': 253,
        'bri': 254
    },
    300: {
        'description': 'light intensity drizzle',
        "on": True,
        'hue': 46638,
        'sat': 253,
        'bri': 254
    },
    301: {
        'description': 'drizzle',
        "on": True,
        'hue': 46638,
        'sat': 253,
        'bri': 254
    },
    302: {
        'description': 'heavy intensity drizzle',
        "on": True,
        'hue': 46638,
        'sat': 253,
        'bri': 254
    },
    310: {
        'description': 'light intensity drizzle rain',
        "on": True,
        'hue': 46638,
        'sat': 253,
        'bri': 254
    },
    311: {
        'description': 'drizzle rain',
        "on": True,
        'hue': 46638,
        'sat': 253,
        'bri': 254
    },
    312: {
        'description': 'heavy intensity drizzle rain',
        "on": True,
        'hue': 46638,
        'sat': 253,
        'bri': 254
    },
    321: {
        'description': 'shower drizzle',
        'hue': 46638,
        "on": True,
        'sat': 253,
        'bri': 254
    },
    500: {
        'description': 'light rain',
        "on": True,
        'hue': 46638,
        'sat': 253,
        'bri': 254
    },
    501: {
        'description': 'moderate rain',
        "on": True,
        'hue': 46638,
        'sat': 253,
        'bri': 254
    },
    502: {
        'description': 'heavy intensity rain',
        "on": True,
        'hue': 46638,
        'sat': 253,
        'bri': 254
    },
    503: {
        'description': 'very heavy rain',
        "on": True,
        'hue': 46638,
        'sat': 253,
        'bri': 254
    },
    504: {
        'description': 'extreme rain',
        "on": True,
        'hue': 46638,
        'sat': 253,
        'bri': 254
    },
    511: {
        'description': 'freezing rain',
        "on": True,
        'hue': 46638,
        'sat': 253,
        'bri': 254
    },
    520: {
        'description': 'light intensity shower rain',
        "on": True,
        'hue': 46638,
        'sat': 253,
        'bri': 254
    },
    521: {
        'description': 'shower rain',
        "on": True,
        'hue': 46638,
        'sat': 253,
        'bri': 254
    },
    522: {
        'description': 'heavy intensity shower rain',
        "on": True,
        'hue': 46638,
        'sat': 253,
        'bri': 254
    },
    600: {
        'description': 'light snow',
        "on": True,
        'hue': '',
        'sat': '',
        'bri': ''
    },
    601: {
        'description': 'snow',
        "on": True,
        'hue': 34495,
        'sat': 232,
        'bri': 203
    },
    602: {
        'description': 'heavy snow',
        "on": True,
        'hue': '',
        'sat': '',
        'bri': ''
    },
    611: {
        'description': 'sleet',
        "on": True,
        'hue': '',
        'sat': '',
        'bri': ''
    },
    621: {
        'description': 'shower snow',
        "on": True,
        'hue': '',
        'sat': '',
        'bri': ''
    },
    701: {
        'description': 'mist',
        "on": True,
        'hue': 46638,
        'sat': 253,
        'bri': 254
    },
    711: {
        'description': 'smoke',
        "on": True,
        'hue': '',
        'sat': '',
        'bri': ''
    },
    721: {
        'description': 'haze',
        "on": True,
        'hue': '',
        'sat': '',
        'bri': ''
    },
    731: {
        'description': 'sand/dust whirls',
        "on": True,
        'hue': '',
        'sat': '',
        'bri': ''
    },
    741: {
        'description': 'fog',
        "on": True,
        'hue': '',
        'sat': '',
        'bri': ''
    },
    800: {
        'description': 'sky is clear',
        "on": True,
        'hue': '',
        'sat': '',
        'bri': ''
    },
    801: {
        'description': 'few clouds',
        "on": True,
        'hue': '',
        'sat': '',
        'bri': ''
    },
    802: {
        'description': 'scattered clouds',
        "on": True,
        'hue': '',
        'sat': '',
        'bri': ''
    },
    803: {
        'description': 'broken clouds',
        "on": True,
        'hue': '',
        'sat': '',
        'bri': ''
    },
    804: {
        'description': 'overcast clouds',
        "on": True,
        'hue': '',
        'sat': '',
        'bri': ''
    },
    900: {
        'description': 'tornado',
        "on": True,
        'hue': '',
        'sat': '',
        'bri': ''
    },
    901: {
        'description': 'tropical storm',
        "on": True,
        'hue': '',
        'sat': '',
        'bri': ''
    },
    902: {
        'description': 'hurricane',
        "on": True,
        'hue': '',
        'sat': '',
        'bri': ''
    },
    903: {
        'description': 'cold',
        "on": True,
        'hue': '',
        'sat': '',
        'bri': ''
    },
    904: {
        'description': 'hot',
        "on": True,
        'hue': '',
        'sat': '',
        'bri': ''
    },
    905: {
        'description': 'windy',
        "on": True,
        'hue': '',
        'sat': '',
        'bri': ''
    },
    906: {
        'description': 'hail',
        "on": True,
        'hue': '',
        'sat': '',
        'bri': ''
    },
}
