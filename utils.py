import json
import logging
import multiprocessing
from functools import partial

import requests

from constants import (ALL_LIGHTS_GROUP_ID, GROUP_STATE_URL, LIGHT_IDS,
                       LIGHT_URL, LIGHT_STATE_URL)


POOL_SIZE = multiprocessing.cpu_count() * 2
# Keys we should not send in a PUT.  These are stripped from the GET
# response that contains the full state of the bulb.
REMOVABLE_PUT_KEYS = ('colormode', 'alert', 'ct', 'effect', 'hue', 'reachable',
                      'sat')


def get_light_state(light_id):
    """
    Returns the current state for the provided light ID
    """
    try:
        response = requests.get(LIGHT_URL.format(light_id))
    except requests.exceptions.RequestException:
        return (light_id, None)
    light_state = response.json()['state']

    # If the light is off, don't change any other color settings
    if not light_state['on']:
        light_state = {'on': False}

    # Remove any alert and add a transition time
    light_state['transitiontime'] = 60
    return (light_id, light_state)


def get_light_states():
    """
    Returns all lights current state as a dict
    """
    pool = multiprocessing.Pool(processes=POOL_SIZE)
    light_states = pool.map(get_light_state, LIGHT_IDS)
    pool.close() # no more tasks to process
    pool.join() # wrap up current tasks
    light_states = {id: state for id, state in light_states}

    return light_states


def set_light_state(light_id, json_data=None, prior_states=None):
    """
    Changes the state for the provided light id
    """
    if prior_states:
        light_id = str(light_id) if light_id not in prior_states else light_id
        prior_state = prior_states[light_id]
        json_data = json.dumps(prior_state)
    try:
        r = requests.put(LIGHT_STATE_URL.format(light_id), data=json_data,
                         allow_redirects=True)
    except Exception as e:
        # @TODO: figure out showing logging messages in debug server
        logging.exception('Issue updating light state for light_id %s',
                          light_id)


def set_light_states(json_data='', prior_states=None, light_ids=LIGHT_IDS):
    """
    Changes all light states using the provided data
    """
    pool = multiprocessing.Pool(processes=POOL_SIZE)
    if prior_states:
        # filter the prior states to include the minimum amount of
        # information that is needed.  Each item that changes sends a
        # PUT request internally.  Once we send 10 ore more in a second
        # the birdge will start producing internal server errors.  So
        # it's best to send as little information to be updated as possible.
        for light_id in prior_states.keys():
            for key in prior_states[light_id].keys():
                if key in REMOVABLE_PUT_KEYS:
                    prior_states[light_id].pop(key, None)
                # If it was previously on, don't tell it to be on again
                # as it's already on due to the alert.  This will result
                # in a wateful api call.
                if prior_states[light_id].get('on', False):
                    prior_states[light_id].pop('on', None)
        partial_set_light_state = partial(set_light_state,
                                          prior_states=prior_states)
    else:
        partial_set_light_state = partial(set_light_state, json_data=json_data)
    pool.map(partial_set_light_state, light_ids)
    pool.close() # no more tasks to process
    pool.join() # wrap up current tasks


def set_group_state(json_data, group_id=ALL_LIGHTS_GROUP_ID):
    """
    Sets the state of a group
    """
    r = requests.put(GROUP_STATE_URL.format(group_id), data=json_data)
