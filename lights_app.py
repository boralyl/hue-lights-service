from datetime import datetime, timedelta
import json
import logging
from logging.handlers import RotatingFileHandler
import time

import ephem
from flask import Flask, session
import requests
from werkzeug.contrib.fixers import ProxyFix

from constants import (LIGHT_STATE_URL, MAIN_LIGHT_IDS, MAIN_LIGHTS_GROUP_ID,
                       WEATHER_API_URL)
import trimet
from utils import get_light_states, set_light_states, set_group_state
from weather import get_weather


app = Flask(__name__)
app.config.from_pyfile('config.py')
app.secret_key = app.config['APP_SECRET_KEY']


@app.route('/incoming-text')
def incoming_text():
    light_states = get_light_states()
    data = {
        "hue": 56100,
        "on": True,
        "bri": 255,
        "sat": 255,
        "alert": "select"
    }
    set_group_state(json.dumps(data))

    time.sleep(5)

    set_light_states(prior_states=light_states)

    return "Success"


@app.route('/incoming-call')
def incoming_call():
    light_states = get_light_states()
    session['previous_light_states'] = light_states
    data = {
        "hue": 46920,
        "on": True,
        "bri": 255,
        "sat": 255,
        "alert": "lselect"
    }
    set_group_state(json.dumps(data))

    return "Success"


@app.route('/call-answered')
@app.route('/call-missed')
def cancel_alert():
    data = session.get('previous_light_states')
    if data:
        set_light_states(prior_states=data)
    return "Call answered or missed."


@app.route('/wifi-connected')
def wifi_connected():
    observer = ephem.Observer()
    observer.pressure = 0
    observer.elevation = 7 # meters
    observer.horizon = '-0:34'
    observer.lat = '' # your lat goes here.
    observer.lon = '' # your lng goes here
    observer.date = datetime.now()
    sunset = ephem.localtime(observer.next_setting(ephem.Sun()))
    if datetime.now() > (sunset - timedelta(minutes=30)):
        data = {
            "on": True,
            "bri": 254,
            "hue": 13157,
            "sat": 210,
            "xy": [
            	0.5109,
            	0.4148
            ],
            "ct": 463,
            "alert": "none",
            "effect": "none",
        }
        set_group_state(json.dumps(data), group_id=MAIN_LIGHTS_GROUP_ID)
    return str(sunset)


@app.route('/weather-lights')
def weather_lights():
    light_states = get_weather()
    set_light_states(prior_states=light_states, light_ids=MAIN_LIGHT_IDS)
    return "weather!"


@app.route('/trimet')
def trimet_alert():
    app.logger.debug('trimet called')
    if trimet.should_perform_alert():
        light_states = get_light_states()
        data = {
            "on": True,
            "bri": 254,
            "hue": 3855,
            "sat": 254,
            "alert": "lselect",
        }
        set_group_state(json.dumps(data))
        time.sleep(20)
        set_light_states(prior_states=light_states)

    return 'done'


if __name__ == '__main__':
    app.run(debug=True)
