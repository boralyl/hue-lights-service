hue-lights-service
==================

A light weight web application that interacts with the phillips hue bridge API.

Install
-------

1. Install pip-reqs.txt `pip install -r pip-reqs.txt`
2. Start gunicorn `gunicorn --workers=2 --daemon -b 127.0.0.1:8888 lights_app:app`

Features
--------

* Flash lights blue on an incoming call (use [tasker](https://play.google.com/store/apps/details?id=net.dinglisch.android.taskerm&hl=en) to hit url when incoming call occurs on your phone)
* Pink alert flash on an incoming text message (use [tasker](https://play.google.com/store/apps/details?id=net.dinglisch.android.taskerm&hl=en) to hit url when incoming text occurs on your phone)
* Return lights to previous state after call is missed or answered. (use [tasker](https://play.google.com/store/apps/details?id=net.dinglisch.android.taskerm&hl=en) to hit url when these events happen on your phone)
* Turn lights on when your phone connects to your wifi if it is after dusk (use [tasker](https://play.google.com/store/apps/details?id=net.dinglisch.android.taskerm&hl=en) to hit url when wifi is connected on your phone)
* Change lights to describe weather (will it rain (blue/white), temperature(blue - cold, white - mild, orange - warm, red - hot)
* Trimet bus alert flashes lights orange 7 minutes prior to arrival at a stop. 
